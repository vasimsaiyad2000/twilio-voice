package com.demo.twilio;

import java.util.Date;

public class Transcription {

	private String sid;
	private String transcriptionText;
	private String status;
	private Date createdDate;
	private int duration;
	private String audioUrl;
	
	
	public String getSid() {
		return sid;
	}
	public String getTranscriptionText() {
		return transcriptionText;
	}
	public String getStatus() {
		return status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public void setTranscriptionText(String transcriptionText) {
		this.transcriptionText = transcriptionText;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public String getAudioUrl() {
		return audioUrl;
	}
	public void setAudioUrl(String audioUrl) {
		this.audioUrl = audioUrl;
	}
}
