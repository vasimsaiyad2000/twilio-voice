package com.demo.twilio;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import com.twilio.sdk.client.TwilioCapability;
import com.twilio.sdk.client.TwilioCapability.DomainException;

/**
 * Servlet implementation class TwilioServlet
 */

@WebServlet(name = "Get client auth token", value = "/client")
public class TwilioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public static final String ACCOUNT_SID = "ACd56aec82c7d8591912a6171aa560440e";
	public static final String AUTH_TOKEN = "ac366fc0f96ac5077d2ef836ccd525c0";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TwilioServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    //https://www.twilio.com/console/voice/dev-tools/twiml-apps/APc18ab4456cc632c68f9092416b6e4f2d
    }
	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		TwilioCapability capability = new TwilioCapability(ACCOUNT_SID, AUTH_TOKEN);
		capability.allowClientOutgoing("APc18ab4456cc632c68f9092416b6e4f2d");
		capability.allowClientIncoming("alice");
		
		String token = null;
		try {
			token = capability.generateToken();
		} catch (DomainException e) {
			e.printStackTrace();
		}
		
		response.setContentType("text/html");
        request.setAttribute("token", token);
        
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        view.forward(request, response);
	}
}
