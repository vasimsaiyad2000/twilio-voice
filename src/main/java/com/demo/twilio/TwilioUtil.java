package com.demo.twilio;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.instance.Recording;


public class TwilioUtil {

	private static final String ACCOUNT_SID = "ACd56aec82c7d8591912a6171aa560440e";
	private static final String AUTH_TOKEN = "ac366fc0f96ac5077d2ef836ccd525c0";
	private static final String TWILIO_API_URL = "https://api.twilio.com";

	  public static List<Transcription> getTranscriptions(){
		  List<Transcription> transcriptions = new ArrayList<Transcription>();
		  
		  for(com.twilio.sdk.resource.instance.Transcription transcription : getClient().getAccount().getTranscriptions()){
			  
			  Transcription t = new Transcription();
			  t.setSid(transcription.getSid());
			  t.setStatus(transcription.getStatus());
			  t.setTranscriptionText(transcription.getTranscriptionText());
			  t.setCreatedDate(transcription.getDateCreated());
			  t.setCreatedDate(transcription.getDateCreated());
			  t.setDuration(transcription.getDuration());
			
			  String audioUrl = getAudioUrl(transcription.getRecordingSid());
			  
			  if(StringUtils.isNotBlank(audioUrl)){
				  t.setAudioUrl(audioUrl);
			  }
			  
			  transcriptions.add(t);
		  }
		  
		  return transcriptions;
	  }
	  
	  public static String getAudioUrl(String callId){
		  String audioUrl = null;
		  
		  Recording recording = getClient().getAccount().getRecording(callId);
		  
		  if(recording != null){
			audioUrl =   recording.getProperty("uri").substring(0, recording.getProperty("uri").indexOf("."));
		  }
		  
		  return String.format("%s%s", TWILIO_API_URL, audioUrl);
	  }
	  
	  public static Transcription getTranscription(String transcriptionId){
		  
		  Transcription t = null;
		  com.twilio.sdk.resource.instance.Transcription transcription = getClient().getAccount().getTranscription(transcriptionId);
		  
		  if(transcription != null){
			  t = new Transcription();  
			  t.setSid(transcription.getSid());
			  t.setStatus(transcription.getStatus());
			  t.setTranscriptionText(transcription.getTranscriptionText());
			  t.setCreatedDate(transcription.getDateCreated());
			  t.setDuration(transcription.getDuration());
			  
			  String audioUrl = getAudioUrl(transcription.getRecordingSid());
			  
			  if(StringUtils.isNotBlank(audioUrl)){
				  t.setAudioUrl(audioUrl);
			  }
		  }
		  
		  return t;
	  }
	  
	  private static TwilioRestClient getClient(){
		  return new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);  
	  }
}
