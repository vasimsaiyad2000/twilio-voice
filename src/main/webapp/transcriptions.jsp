<%@page import="com.demo.twilio.Transcription"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Transcription List</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<script type="text/javascript"
          src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
</script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
<style type="text/css">
    .bs-example{
    	margin: 20px;
}
</style>
<script type="text/javascript">
function showTranscription(sid){
	
	$('#myModal').modal('show');
	$('.modal-body').html('<h1>Processing...</h1>');
	
	$.ajax({
		url : 'transcription?sid=' + sid,
		success : function(response) {
			if(response){
				
				$('.modal-body').html('');
				
				var html = '<table class="table table-bordered">';
				html +='<tbody>';
				html +='<tr>';
				html +='<td>Transcription ID</td>';
				html +='<td>' + response.sid + '</td>';
				html +='</tr>';
				
				html +='<tr>';
				html +='<td>Text</td>';
				html +='<td>' + response.transcriptionText + '</td>';
				html +='</tr>';
				
				html +='<tr>';
				html +='<td>Duration</td>';
				html +='<td>' + response.duration + ' secs </td>';
				html +='</tr>';
				
				html +='<tr>';
				html +='<td>Status</td>';
				html +='<td>' + response.status + '</td>';
				html +='</tr>';
				
				html +='<tr>';
				html +='<td>Created Date </td>';
				html +='<td>' + response.createdDate + '</td>';
				html +='</tr>';
				
				html +='</tbody></table>';
				
				$('.modal-body').html(html);			
			}
		}
	}); 
}
</script>
</head>
<body>
<div class="bs-example">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Transcription ID</th>
                <th>Duration</th>
                <th>Status</th>
                <th>Created Date</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <%
        	List<Transcription> list = (List<Transcription>) request.getAttribute("transcriptions");
        	for(Transcription transcription : list){
        		
        %>
            <tr>
                <td><%=transcription.getSid() %></td>
                <td ><%=transcription.getDuration() %> secs</td>
                <td><%=transcription.getStatus() %></td>
                <td><%=transcription.getCreatedDate() %></td>
                <td>
                	<audio id="player_<%=transcription.getSid() %>" src="<%=transcription.getAudioUrl() %>"></audio>
                	<button type="button" class="btn btn-link" onclick="document.getElementById('player_' + '<%=transcription.getSid() %>').play()">
  						Play
					</button>
					
					<button type="button" class="btn btn-link" onclick="document.getElementById('player_' + '<%=transcription.getSid() %>').pause()">
  						Pause
					</button>
					
					<button type="button" class="btn btn-link" data-toggle="modal" onclick = "showTranscription('<%=transcription.getSid()%>');">
  						View
					</button>
                </td>
            </tr>
        <%
        	}
        %>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Twilio Voice Service</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>